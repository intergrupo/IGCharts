//
//  IGChart.h
//  IGCharts
//
//  Created by Jhon Jaiver López Calderón on 1/11/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"

@interface IGChart : NSObject

#pragma mark - Constans
#define POSITION_TOP @"top"
#define POSITION_BOTTOM @"bottom"
#define POSITION_LEFT @"left"
#define POSITION_RIGHT @"right"

#pragma mark - Store
@property (nonatomic, strong) NSMutableDictionary *chartConfig;
@property (nonatomic, strong) NSMutableArray *store;
@property (nonatomic, strong) NSArray *fields;

#pragma mark - Config Values
@property (nonatomic, strong) NSString *chartType;
@property (nonatomic, strong) NSNumber *animate;
@property (nonatomic, strong) NSNumber *hasShadow;
@property (nonatomic, strong) NSNumber *legendIsDocked;
@property (nonatomic, strong) NSString *xTitle;
@property (nonatomic, strong) NSString *yTitle;
@property (nonatomic, strong) NSNumber *xAxeMax;
@property (nonatomic, strong) NSNumber *xAxeMin;
@property (nonatomic, strong) NSArray *seriesConfig;


- (NSString*) configAsJSON;
- (void) addSerieForIndex:(NSArray*) serieData;
@end
