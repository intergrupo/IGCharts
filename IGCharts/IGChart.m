//
//  IGChart.m
//  IGCharts
//
//  Created by Jhon Jaiver López Calderón on 1/11/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import "IGChart.h"

@implementation IGChart

@synthesize chartType, store, fields, seriesConfig, chartConfig;
@synthesize animate, hasShadow, legendIsDocked, xTitle, yTitle, xAxeMax, xAxeMin;

- (id)init {
    if ( self = [super init] )
    {
        chartConfig = [[NSMutableDictionary alloc] init];
        store = [[NSMutableArray alloc] init];
        animate = [NSNumber numberWithBool:YES];
        hasShadow = [NSNumber numberWithBool:YES];
        xAxeMax = [[NSNumber alloc] initWithInteger:100];
        xAxeMin = [[NSNumber alloc] initWithInteger:0];
    }
    return self;
}

/*
 * Returns the config dictionary in JSON format
 */
-(NSString*) configAsJSON {
    
    [chartConfig setObject:chartType forKey:@"chartType"];
    
    if (store != nil) {
        [chartConfig setObject:store forKey:@"dataStore"];
    }
    
    if(fields != nil){
        [chartConfig setObject:fields forKey:@"fields"];
    }
    
    if(seriesConfig != nil){
        [chartConfig setObject:seriesConfig forKey:@"seriesConfig"];
    }
    
    if(xTitle != nil){
        [chartConfig setObject:xTitle forKey:@"xTitle"];
    }
    
    if(yTitle != nil){
        [chartConfig setObject:yTitle forKey:@"yTitle"];
    }
    
    [chartConfig setObject:xAxeMax forKey:@"xMax"];
    [chartConfig setObject:xAxeMin forKey:@"xMin"];
    
    if ([animate boolValue]) {
        [chartConfig setObject:@"true" forKey:@"animate"];
    } else {
        [chartConfig setObject:@"false" forKey:@"animate"];
    }
    
    if ([hasShadow boolValue]) {
        [chartConfig setObject:@"true" forKey:@"hasShadow"];
    } else {
        [chartConfig setObject:@"false" forKey:@"hasShadow"];
    }
    
    NSString* json = [chartConfig JSONRepresentation];
 
    return json;
}

/*
 * Method to add a dictionary serie for the series array
 */
- (void) addSerieForIndex:(NSArray*) serieData{
    NSMutableDictionary *serieDict = [[NSMutableDictionary alloc] initWithCapacity:[fields count]];
    
    for (int i = 0; i < [fields count]; i++) {
        [serieDict setObject:[serieData objectAtIndex:i] forKey:[fields objectAtIndex:i]];
    }
    //Add dictionary with serie data to the store array
    [store addObject:serieDict];
}

- (NSNumber*) randomValue {
    NSInteger a = arc4random() % 100;
    return [NSNumber numberWithInt:a];
}

@end
