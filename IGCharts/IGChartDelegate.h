//
//  IGChartDelegate.h
//  IGCharts
//
//  Created by Julian David Duque Jaramillo on 1/31/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IGChart.h"


@interface IGChartDelegate : NSObject {
}

@property (strong, nonatomic) IGChart *chartConfig;

#pragma mark - Inits
- (id) initWithChartConfig:(IGChart *)chart;
- (void) setup:(UIWebView *) webView;
- (void) load:(UIWebView *) webView;

@end
