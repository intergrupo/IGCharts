//
//  IGChartDelegate.m
//  IGCharts
//
//  Created by Julian David Duque Jaramillo on 1/31/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import "IGChartDelegate.h"

@implementation IGChartDelegate

@synthesize chartConfig;

//Custom init
- (id) initWithChartConfig:(IGChart *)chart {
    self = [super init];
    if(self){
        [self setChartConfig:chart];
    }
    return self;
}

- (void)setup:(UIWebView *)webView {
    NSString *indexPath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"];
    NSData *fileData = [NSData dataWithContentsOfFile:indexPath];
    NSString *jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    
    [webView loadHTMLString:jsString baseURL:nil];
}


#pragma mark - WebView delegate methods
- (void)load:(UIWebView *)webView {
        
    
    NSString *stylePath = [[NSBundle mainBundle] pathForResource:@"touch-charts-demo" ofType:@"css"];
    NSData *fileData = [NSData dataWithContentsOfFile:stylePath];
    NSString *jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    NSString *senchaTouchPath = [[NSBundle mainBundle] pathForResource:@"sencha-touch" ofType:@"js"];
    fileData = [NSData dataWithContentsOfFile:senchaTouchPath];
    jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    NSString *senchaChartsPath = [[NSBundle mainBundle] pathForResource:@"touch-charts" ofType:@"js"];
    fileData = [NSData dataWithContentsOfFile:senchaChartsPath];
    jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
    
    [webView stringByEvaluatingJavaScriptFromString:jsString];
    
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"drawChart(%@)", [self.chartConfig configAsJSON]]];
    
}

@end
