//
//  IGChartPanel.h
//  IGCharts
//
//  Created by Jhon Jaiver López Calderón on 1/11/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGChartPanel : NSObject

@property (nonatomic, strong) NSArray *chartsArray;
@property (nonatomic, strong) NSString *title;

@end
