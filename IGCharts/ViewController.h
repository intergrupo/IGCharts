//
//  ViewController.h
//  IGCharts
//
//  Created by Jhon Jaiver López Calderón on 1/10/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGChart.h"
#import "IGChartDelegate.h"

@interface ViewController : UIViewController<UIWebViewDelegate> {
    IGChartDelegate *igChart;
}

@property (strong, nonatomic) IBOutlet UIWebView *chartWebView;
@property (strong, nonatomic) IGChart *chartConfig;


- (IGChart *) configLineChart;
- (IGChart *) configPieChart;

- (NSNumber*) randomValue;
@end
