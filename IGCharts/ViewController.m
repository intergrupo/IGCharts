//
//  ViewController.m
//  IGCharts
//
//  Created by Jhon Jaiver López Calderón on 1/10/12.
//  Copyright (c) 2012 IGApps. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize chartWebView, chartConfig;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    igChart = [[IGChartDelegate alloc] initWithChartConfig:[self configPieChart]];
    [igChart setup:chartWebView];
    chartWebView.delegate = self;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [igChart load:webView];
}

- (IGChart *) configLineChart{
    IGChart *lineChart = [[IGChart alloc] init];
    
    //No default value for chart type!
    lineChart.chartType = @"line";
    
    lineChart.xTitle = @"Month of the Year";
    lineChart.yTitle = @"Number of Hits";
    
    //Default true
    lineChart.animate = [NSNumber numberWithBool:NO];
    //Default true
    lineChart.hasShadow = [NSNumber numberWithBool:NO];
    //Default 100
    lineChart.xAxeMax = [[NSNumber alloc] initWithInt:100];
    //Default 0
    lineChart.xAxeMin = [[NSNumber alloc] initWithInt:0];
    
    
    //Name of the fields. First one is xAxe name. The rest are yAxe values names
    lineChart.fields = [[NSArray alloc] initWithObjects:@"month", @"iPhone",@"iPad", @"Android", @"BlackBerry", nil];
    
    lineChart.seriesConfig = [NSArray arrayWithObjects:
                              [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"iPhone", @"false", @"false", nil] forKeys:[NSArray arrayWithObjects:@"fieldName", @"fill", @"smooth", nil]],
                              [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"iPad", @"false", @"false", nil] forKeys:[NSArray arrayWithObjects:@"fieldName", @"fill", @"smooth", nil]],
                              [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Android", @"false", @"true", nil] forKeys:[NSArray arrayWithObjects:@"fieldName", @"fill", @"smooth", nil]],
                              [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"BlackBerry", @"true", @"true", nil] forKeys:[NSArray arrayWithObjects:@"fieldName", @"fill", @"smooth", nil]],
                              nil];
    
    //The number of values in the array must match with number of objects in fields array 
    NSArray *dataForSerie = [[NSArray alloc] initWithObjects:@"January", [self randomValue],[self randomValue],[self randomValue],[self randomValue], nil];
    [lineChart addSerieForIndex:dataForSerie];
    
    dataForSerie = [[NSArray alloc] initWithObjects:@"February", [self randomValue],[self randomValue],[self randomValue],[self randomValue], nil];
    [lineChart addSerieForIndex:dataForSerie];
    
    dataForSerie = [[NSArray alloc] initWithObjects:@"March", [self randomValue],[self randomValue],[self randomValue],[self randomValue], nil];
    [lineChart addSerieForIndex:dataForSerie];
    
    dataForSerie = [[NSArray alloc] initWithObjects:@"April", [self randomValue],[self randomValue],[self randomValue],[self randomValue], nil];
    [lineChart addSerieForIndex:dataForSerie];
    return lineChart;
}


- (IGChart *) configPieChart{
    IGChart *pieChart = [[IGChart alloc] init];
    
    //No default value for chart type!
    pieChart.chartType = @"pie";
    
    //Default true
    pieChart.animate = [NSNumber numberWithBool:YES];
    //Default true
    pieChart.hasShadow = [NSNumber numberWithBool:NO];

    
    //Name of the fields. 
    pieChart.fields = [[NSArray alloc] initWithObjects:@"name", @"iPhone", nil];
    
    //The number of values in the array must match with number of objects in fields array 
    NSArray *dataForSerie = [[NSArray alloc] initWithObjects:@"January", [self randomValue], nil];
    [pieChart addSerieForIndex:dataForSerie];
    
    dataForSerie = [[NSArray alloc] initWithObjects:@"February", [self randomValue], nil];
    [pieChart addSerieForIndex:dataForSerie];
    
    dataForSerie = [[NSArray alloc] initWithObjects:@"March", [self randomValue], nil];
    [pieChart addSerieForIndex:dataForSerie];
    
    dataForSerie = [[NSArray alloc] initWithObjects:@"April", [self randomValue], nil];
    [pieChart addSerieForIndex:dataForSerie];
    
    
    return pieChart;
}

- (NSNumber*) randomValue {
    NSInteger a = arc4random() % 100;
    return [NSNumber numberWithInt:a];
}

- (void)viewDidUnload
{
    [self setChartWebView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

//- (void)dealloc {
//    [chartWebView release];
//    [super dealloc];
//}

@end
